package com.pixelr.rule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RuleRepository {
    @Autowired
    DynamoDbMapper dynamoDbMapper;

    public void createAndUpdateRuleInfo(RuleDataDto ruleDataDto){
        dynamoDbMapper.save(ruleDataDto);
    }

    public RuleDataDto getRuleInfo(String ruleId){
        return dynamoDbMapper.load(RuleDataDto.class, ruleId);
    }

    public void deleteRuleInfo(RuleDataDto ruleDataDto){
        dynamoDbMapper.delete(ruleDataDto);
    }
}
