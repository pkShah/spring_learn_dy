package com.pixelr.rule;


@Data
@DynamoDBTable(tableName = "rule_table")
public class ProductInfo {

    @DynamoDBHashKey(attributeName = "Id")
    private String rule_id;

    @DynamoDBAttribute(attributeName="data")
    private String data;

    @DynamoDBAttribute(attributeName="gid")
    private String gid;

    @DynamoDBAttribute(attributeName="initiator_id")
    private String initiator_id;
}